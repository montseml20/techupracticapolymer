#Comentarios
#Imagen Raiz
FROM node

#Carpeta raiz
WORKDIR /techufront

ADD /build/default /techufront/build/default

#dependencias
RUN npm install -g bower
RUN npm install -g polymer-cli --unsafe-perm

#Exponer puerto
EXPOSE 8081


#Comando de inicialización
CMD ["polymer", "serve", "--hostname", "0.0.0.0", "--port", "8081", "build/default"]
